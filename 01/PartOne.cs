using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DayOne
{
    public static class PartOne
    {
        public static int Run(string[] input)
        {
            return input
                .Select(line => Int32.Parse(line))
                .Sum();
        }
    }
}