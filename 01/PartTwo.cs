using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DayOne
{
    public static class PartTwo
    {
        public static int Run(string[] input)
        {
            var lines = input
                .Select(line => Int32.Parse(line))
                .ToList();

            var visited = new HashSet<int>();
            var currentFrequency = 0;
            int? result = null;

            while (result == null)
            {
                foreach (var line in lines)
                {
                    if (!visited.Add(currentFrequency))
                    {
                        result = currentFrequency;
                        break;
                    }

                    currentFrequency += line;
                }
            }

            return (int)result;
        }
    }
}