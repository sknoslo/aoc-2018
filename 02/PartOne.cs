using System;
using System.Collections.Generic;
using System.Linq;

namespace DayTwo
{
    public static class PartOne
    {
        public static int Run(string[] input)
        {
            var boxIds = input.Select(line => line.ToCharArray());

            var doubles = boxIds
                .Where(id => HasDuplicates(id, 2))
                .Count();

            var triples = boxIds
                .Where(id => HasDuplicates(id, 3))
                .Count();

            return doubles * triples;
        }

        private static bool HasDuplicates(char[] id, int desiredCount)
        {
            return id.Aggregate(
                new Dictionary<char, int>(),
                (dict, letter) =>
                {
                    int count;

                    if (dict.TryGetValue(letter, out count))
                    {
                        dict.Remove(letter);
                    }

                    dict.Add(letter, count + 1);
                    return dict;
                }
            ).ContainsValue(desiredCount);
        }
    }
}