using System.Collections.Generic;
using System.Linq;

namespace DayTwo
{
    public static class PartTwo
    {
        public static string Run(string[] input)
        {
            return FindAnswer(input.FirstOrDefault(), new List<string>(input.Skip(1)));
        }

        public static string FindAnswer(string id, List<string> otherIds)
        {
            foreach (var otherId in otherIds)
            {
                if (Diff(id, otherId) == 1)
                {
                    return Shared(id, otherId);
                }
            }

            return FindAnswer(otherIds.FirstOrDefault(), new List<string>(otherIds.Skip(1)));
        }

        public static int Diff(string a, string b)
        {
            var diff = 0;

            for (var i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i]) diff++;
            }

            return diff;
        }

        public static string Shared(string a, string b)
        {
            var shared = new List<char>();

            for (var i = 0; i < a.Length; i++)
            {
                if (a[i] == b[i]) shared.Add(a[i]);
            }

            return new string(shared.ToArray());
        }
    }
}