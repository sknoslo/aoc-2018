using System.Text.RegularExpressions;

namespace DayThree
{
    public class Claim
    {
        private static Regex CLAIM_REGEX = new Regex(@"#(\d+)\s@\s(\d+),(\d+):\s(\d+)x(\d+)");

        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int W { get; set; }
        public int H { get; set; }

        public override string ToString()
        {
            return $"{X},{Y}: {W}x{H}";
        }

        public static Claim FromString(string input)
        {
            var match = CLAIM_REGEX.Match(input);

            return new Claim
            {
                Id = int.Parse(match.Groups[1].Value),
                X = int.Parse(match.Groups[2].Value),
                Y = int.Parse(match.Groups[3].Value),
                W = int.Parse(match.Groups[4].Value),
                H = int.Parse(match.Groups[5].Value)
            };
        }
    }
}