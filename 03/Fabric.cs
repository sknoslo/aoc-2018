using System;

namespace DayThree
{
    public class Fabric
    {
        private char[,] data = new char[1000, 1000];

        public Fabric()
        {
            for (var x = 0; x < 1000; x++)
            {

                for (var y = 0; y < 1000; y++)
                {
                    data[x, y] = '.';
                }
            }
        }

        public void PlotClaim(Claim claim)
        {
            for (var x = claim.X; x < claim.X + claim.W; x++)
            {
                for (var y = claim.Y; y < claim.Y + claim.H; y++)
                {
                    if (data[x, y] == '.')
                    {
                        data[x, y] = '#';
                    }
                    else
                    {
                        data[x, y] = 'X';
                    }
                }
            }
        }

        public int OverlapCount()
        {
            var count = 0;

            for (var x = 0; x < 1000; x++)
            {

                for (var y = 0; y < 1000; y++)
                {
                    if (data[x, y] == 'X') count++;
                }
            }

            return count;
        }

        public bool IsClaimPristine(Claim claim)
        {
            for (var x = claim.X; x < claim.X + claim.W; x++)
            {
                for (var y = claim.Y; y < claim.Y + claim.H; y++)
                {
                    if (data[x, y] == 'X')
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void Draw()
        {
            for (var x = 0; x < 25; x++)
            {

                for (var y = 0; y < 25; y++)
                {
                    Console.Write(data[x, y]);
                }
                Console.WriteLine();
            }
        }
    }
}