using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace DayThree
{
    public static class PartOne
    {
        public static int Run(string[] input)
        {
            var claims = input.Select(line => Claim.FromString(line));
            var fabric = new Fabric();

            foreach (var claim in claims)
            {
                fabric.PlotClaim(claim);
            }

            return fabric.OverlapCount();
        }
    }
}