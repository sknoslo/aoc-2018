using System.Linq;

namespace DayThree
{
    public static class PartTwo
    {
        public static int Run(string[] input)
        {
            var claims = input.Select(line => Claim.FromString(line));
            var fabric = new Fabric();

            foreach (var claim in claims)
            {
                fabric.PlotClaim(claim);
            }

            foreach (var claim in claims)
            {
                if (fabric.IsClaimPristine(claim))
                {
                    return claim.Id;
                }
            }

            return -1;
        }
    }
}