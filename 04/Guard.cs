namespace DayFour
{
    public class Guard
    {
        public int Id { get; }

        public int TotalMinutesSlept { get; private set; } = 0;
        private int[] minutesSlept = new int[60];
        private int? asleepSince = null;

        public Guard(int id)
        {
            Id = id;
        }

        public void Sleep(int minute)
        {
            asleepSince = minute;
        }

        public void Wakeup(int minute)
        {
            TotalMinutesSlept += minute - asleepSince.Value;
            for (var i = asleepSince.Value; i < minute; i++)
            {
                minutesSlept[i]++;
            }
            asleepSince = null;
        }

        public int FindSleepiestMinute()
        {
            var sleepiest = 0;

            for (var i = 1; i < 60; i++)
            {
                if (minutesSlept[i] > minutesSlept[sleepiest])
                {
                    sleepiest = i;
                }
            }

            return sleepiest;
        }

        public int FindSleepiestMinuteFrequency()
        {
            return minutesSlept[FindSleepiestMinute()];
        }
    }
}