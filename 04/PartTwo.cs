using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DayFour
{
    public static class PartTwo
    {
        public static int Run(string[] input)
        {
            var sortedList = input.ToList();
            sortedList.Sort();

            var records = sortedList
                .Select(line => Record.FromString(line))
                .ToList();

            var guards = new Dictionary<int, Guard>();
            Guard currentGuard = null;
            Guard sleepiestGuard = null;

            foreach (var record in records)
            {
                if (record.IsGuardChangeEvent)
                {
                    var guardId = record.GuardId;

                    if (guards.ContainsKey(guardId))
                    {
                        currentGuard = guards[guardId];
                    }
                    else
                    {
                        currentGuard = new Guard(guardId);
                        guards.Add(guardId, currentGuard);
                    }
                }
                else if (record.IsSleepEvent)
                {
                    currentGuard.Sleep(record.Minute);
                }
                else if (record.IsAwakeEvent)
                {
                    currentGuard.Wakeup(record.Minute);

                    if (sleepiestGuard == null || currentGuard.FindSleepiestMinuteFrequency() > sleepiestGuard.FindSleepiestMinuteFrequency())
                    {
                        sleepiestGuard = currentGuard;
                    }
                }
            }

            return sleepiestGuard.Id * sleepiestGuard.FindSleepiestMinute();
        }
    }
}