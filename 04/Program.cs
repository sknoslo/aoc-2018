﻿using System;
using System.IO;

namespace DayFour
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines("input.txt");

            Console.WriteLine("##### Day Four #####");
            Console.WriteLine($"Part 1: {PartOne.Run(input)}");
            Console.WriteLine($"Part 2: {PartTwo.Run(input)}");
        }
    }
}
