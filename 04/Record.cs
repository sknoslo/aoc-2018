using System.Text.RegularExpressions;

namespace DayFour
{
    public class Record
    {
        private static Regex RECORD_REGEX = new Regex(
            @"^\[(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2})\]\s(.*)$");
        private static Regex GUARD_ID_REGEX = new Regex(@"^Guard #(\d+).*$");
        public int Minute { get; set; }
        public string Event { get; set; }

        public bool IsGuardChangeEvent => Event.StartsWith("Guard");
        public int GuardId => int.Parse(Record.GUARD_ID_REGEX.Match(Event).Groups[1].Value);
        public bool IsAwakeEvent => Event.StartsWith("wakes");
        public bool IsSleepEvent => Event.StartsWith("falls");

        public static Record FromString(string record)
        {
            var match = RECORD_REGEX.Match(record);

            return new Record
            {
                Minute = int.Parse(match.Groups[5].Value),
                Event = match.Groups[6].Value
            };
        }
    }
}