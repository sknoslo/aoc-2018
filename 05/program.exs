defmodule PartOne do
  def run(input) do
    input
    |> Solver.solve()
    |> length()
  end
end

defmodule PartTwo do
  def run(input) do
    ?a..?z
    |> Enum.map(fn polymer -> Task.async(fn -> run(polymer, input) end) end)
    |> Enum.map(fn task -> Task.await(task) end)
    |> Enum.min(0)
  end

  def run(excluded, input) do
    input
    |> Enum.filter(fn x -> !(x == excluded || x + 32 == excluded) end)
    |> Solver.solve()
    |> length()
  end
end

defmodule Integer.Guards do
  defguard opposite_chars(a, b) when abs(a - b) == 32
end

defmodule Solver do
  import Integer.Guards

  def solve(input) do
    List.foldr(input, '', fn step, acc ->
      case {step, acc} do
        {step, [h | t]} when opposite_chars(step, h) -> t
        {step, acc} -> [step | acc]
      end
    end)
  end
end

input =
  "input.txt"
  |> File.read!()
  |> String.trim()
  |> to_charlist()

IO.puts("##### Day Five #####")
IO.puts("Part 1: " <> to_string(PartOne.run(input)))
IO.puts("Part 2: " <> to_string(PartTwo.run(input)))
