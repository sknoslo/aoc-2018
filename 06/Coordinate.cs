using System;
using System.Text.RegularExpressions;

namespace DaySix
{
    public class Coordinate
    {
        private static Regex POINT_REGEX = new Regex(@"^(?<x>\d+),\s(?<y>\d+)$");
        private static char NEXT_LABEL = 'A';

        public char label;
        public int x;
        public int y;

        public static Coordinate FromString(string input)
        {
            var match = POINT_REGEX.Match(input);

            return new Coordinate
            {
                label = ++NEXT_LABEL,
                x = int.Parse(match.Groups["x"].Value),
                y = int.Parse(match.Groups["y"].Value)
            };
        }

        public int DistanceTo(int px, int py)
        {
            return Math.Abs(x - px) + Math.Abs(y - py);
        }
    }
}