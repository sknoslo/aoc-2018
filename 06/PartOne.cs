using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace DaySix
{
    public static class PartOne
    {
        public static int Run(string[] input)
        {
            var coords = input.Select(str => Coordinate.FromString(str)).ToList();

            var minX = coords.Select(coord => coord.x).Min() - 1;
            var maxX = coords.Select(coord => coord.x).Max() + 1;
            var minY = coords.Select(coord => coord.y).Min() - 1;
            var maxY = coords.Select(coord => coord.y).Max() + 1;

            var map = new Dictionary<char, int>();

            for (var x = minX; x <= maxX; x++)
            {
                for (var y = minY; y <= maxY; y++)
                {
                    var infinite = x == minX || x == maxX || y == minY || y == maxY;
                    int? min_dist = null;
                    char? min_label = null;

                    foreach (var coord in coords)
                    {
                        var dist = coord.DistanceTo(x, y);

                        if (dist == 0)
                        {
                            min_dist = 0;
                            min_label = coord.label;
                            break;
                        }

                        if (!min_dist.HasValue)
                        {
                            min_dist = dist;
                            min_label = coord.label;
                        }
                        else if (dist < min_dist)
                        {
                            min_dist = dist;
                            min_label = coord.label;
                        }
                        else if (dist == min_dist)
                        {
                            min_label = '.';
                        }
                    }

                    var label = (char)min_label;
                    if (label == '.') continue;

                    map.TryAdd(label, 0);
                    if (!infinite && map[label] >= 0)
                    {
                        map[label]++;
                    }
                    else
                    {
                        map[label] = -1;
                    }
                }
            }

            return map.Select(k => k.Value).Max();
        }
    }
}