using System.Linq;

namespace DaySix
{
    public static class PartTwo
    {
        public static int Run(string[] input)
        {
            var coords = input.Select(str => Coordinate.FromString(str)).ToList();

            var minX = coords.Select(coord => coord.x).Min() - 1;
            var maxX = coords.Select(coord => coord.x).Max() + 1;
            var minY = coords.Select(coord => coord.y).Min() - 1;
            var maxY = coords.Select(coord => coord.y).Max() + 1;

            var regionSize = 0;

            for (var x = minX; x <= maxX; x++)
            {
                for (var y = minY; y <= maxY; y++)
                {
                    if (coords.Select(c => c.DistanceTo(x, y)).Sum() < 10000)
                    {
                        regionSize++;
                    }
                }
            }

            return regionSize;
        }
    }
}