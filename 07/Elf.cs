namespace DaySeven
{
    public class Elf
    {
        private int workLeft = 0;
        private char? workingOn = null;
        private StepList stepList;

        public bool available => workingOn == null;

        public Elf(StepList stepList) => this.stepList = stepList;

        public void StartStep(char? step)
        {
            if (step == null) return;

            workLeft = 60 + (int)step - 64; // A = 65, adjust to 1
            workingOn = step;
            stepList.Start((char)step);
        }

        public char? Work()
        {
            workLeft--;

            if (workLeft == 0)
            {
                stepList.Finish((char)workingOn);
                var finished = workingOn;
                workingOn = null;

                return finished;
            }

            return null;
        }

        public char ToChar()
        {
            return workingOn.HasValue ? workingOn.Value : '.';
        }
    }
}