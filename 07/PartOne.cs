using System;

namespace DaySeven
{
    public static class PartOne
    {
        public static string Run(string[] input)
        {
            var steps = new StepList();

            foreach (var line in input)
            {
                steps.Insert(line);
            }

            var result = "";
            var step = steps.FindNextUnlockedStep();

            while (step != null)
            {
                result += step;
                steps.Do((char)step);
                step = steps.FindNextUnlockedStep();
            }

            return result;
        }
    }
}