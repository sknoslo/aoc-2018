using System;
using System.Collections.Generic;
using System.Linq;

namespace DaySeven
{
    public static class PartTwo
    {
        public static int Run(string[] input)
        {
            var steps = new StepList();
            var workers = new List<Elf>();

            foreach (var line in input)
            {
                steps.Insert(line);
            }

            for (var i = 0; i < 5; i++)
            {
                workers.Add(new Elf(steps));
            }

            var result = "";
            var second = 0;

            while (result.Length < 26)
            {
                var busyWorkers = workers.Where(worker => !worker.available).ToList();

                foreach (var worker in busyWorkers)
                {
                    var finishedStep = worker.Work();

                    if (finishedStep != null) result += (char)finishedStep;
                }

                var availableWorkers = workers.Where(worker => worker.available).ToList();

                foreach (var worker in availableWorkers)
                {
                    worker.StartStep(steps.FindNextUnlockedStep());
                }

                if (result.Length < 26) second++;
            }

            return second;
        }
    }
}