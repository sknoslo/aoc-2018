﻿using System;
using System.IO;

namespace DaySeven
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines("input.txt");

            Console.WriteLine("##### Day Seven #####");
            Console.WriteLine($"Part 1: {PartOne.Run(input)}");
            Console.WriteLine($"Part 2: {PartTwo.Run(input)}");
        }
    }
}
