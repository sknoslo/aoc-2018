using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DaySeven
{
    public class StepList
    {
        private static Regex step_regex = new Regex(@"^Step\s(\w).*step\s(\w)\scan\sbegin.$");
        private SortedList<char, Step> list = new SortedList<char, Step>();
        public void Insert(string step)
        {
            var match = step_regex.Match(step);
            char label = char.Parse(match.Groups[1].Value);
            char nextStep = char.Parse(match.Groups[2].Value);

            if (!list.ContainsKey(label))
            {
                list.Add(label, new Step(label));
            }

            if (!list.ContainsKey(nextStep))
            {
                list.Add(nextStep, new Step(nextStep));
            }

            list[label].nextSteps.Add(nextStep, nextStep);
            list[nextStep].AddPrereq();
        }

        public void Do(char stepLabel)
        {
            list[stepLabel].status = Step.Status.Done;

            foreach (var next in list[stepLabel].nextSteps)
            {
                list[next.Value].PrereqComplete();
            }
        }

        public void Start(char stepLabel)
        {
            list[stepLabel].status = Step.Status.Working;
        }

        public void Finish(char stepLabel)
        {

            list[stepLabel].status = Step.Status.Done;

            foreach (var next in list[stepLabel].nextSteps)
            {
                list[next.Value].PrereqComplete();
            }
        }

        public void Print()
        {
            foreach (var step in list)
            {
                Console.Write(step.Value.label);
                Console.Write(": ");
                foreach (var next in step.Value.nextSteps)
                {
                    Console.Write($"{next.Key}, ");
                }

                Console.WriteLine();
            }
        }

        public char? FindNextUnlockedStep()
        {
            var next = list.FirstOrDefault(kv => kv.Value.status == Step.Status.Unlocked).Value;

            if (next != null)
            {
                return next.label;
            }

            return null;
        }
    }

    public class Step
    {
        public char label;
        public Status status = Status.Unlocked;
        private int prereqs = 0;

        public SortedList<char, char> nextSteps = new SortedList<char, char>();

        public Step(char label) => this.label = label;

        public void PrereqComplete()
        {
            prereqs--;

            if (prereqs == 0)
            {
                status = Status.Unlocked;
            }
        }

        public void AddPrereq()
        {
            prereqs++;
            status = Status.Locked;
        }

        public enum Status { Locked, Unlocked, Working, Done }
    }
}