using System;
using System.Collections.Generic;
using System.Linq;

namespace DayEight
{
    public static class PartOne
    {
        public static int Run(Stack<int> input)
        {
            var tree = new Tree();

            tree.Populate(input);

            return tree.MetaCounter;
        }
    }
}