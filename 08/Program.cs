﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DayEight
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File
                .ReadAllText("input.txt")
                .Trim()
                .Split(' ')
                .Select(x => int.Parse(x))
                .ToList();

            input.Reverse();

            Console.WriteLine("##### Day Eight #####");
            Console.WriteLine($"Part 1: {PartOne.Run(new Stack<int>(input))}");
            Console.WriteLine($"Part 2: {PartTwo.Run(new Stack<int>(input))}");
        }
    }
}
