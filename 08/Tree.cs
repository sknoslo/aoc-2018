using System;
using System.Collections.Generic;
using System.Linq;

namespace DayEight
{
    public class Tree
    {
        public int MetaCounter { get; private set; } = 0;
        private Node root;

        public int Value()
        {
            return root.Value();
        }

        public void Populate(Stack<int> source)
        {
            root = PopulateRecursive(source);
        }

        private Node PopulateRecursive(Stack<int> source)
        {
            var childCount = source.Pop();
            var metaCount = source.Pop();

            var node = new Node(childCount, metaCount);

            for (var i = 0; i < childCount; i++)
            {
                node.Children[i] = PopulateRecursive(source);
            }

            for (var i = 0; i < metaCount; i++)
            {
                node.MetaData[i] = source.Pop();
                MetaCounter += node.MetaData[i];
            }

            return node;
        }
    }

    public struct Node
    {
        public int ChildCount { get; private set; }
        public int MetaDataCount { get; private set; }
        public Node[] Children { get; }
        public int[] MetaData { get; }

        public Node(int childCount, int metaDataCount)
        {
            ChildCount = childCount;
            MetaDataCount = metaDataCount;
            Children = new Node[childCount];
            MetaData = new int[metaDataCount];
        }

        public int Value()
        {
            var value = 0;

            for (var i = 0; i < MetaDataCount; i++)
            {
                var metaValue = MetaData[i];

                if (ChildCount == 0)
                {
                    value += metaValue;
                }
                else
                {
                    if (metaValue <= ChildCount)
                    {
                        value += Children[metaValue - 1].Value();
                    }
                }
            }

            return value;
        }
    }
}