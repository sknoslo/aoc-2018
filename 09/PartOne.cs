using System;
using System.Collections.Generic;
using System.Linq;

namespace DayNine
{
    public static class PartOne
    {
        public static long Run(int playerCount, int highMarble)
        {
            Console.WriteLine($"{playerCount} players; {highMarble} high marble");
            var game = new Game(playerCount, highMarble * 100);

            game.Simulate();

            return game.GetHighScore();
        }
    }

    public class Game
    {
        private int playerCount;
        private int highMarble;

        private int currentPlayer = 1;
        private int nextMarble = 1;
        private Marble currentMarble;
        private Dictionary<int, long> Scores = new Dictionary<int, long>();

        public Game(int playerCount, int highMarble)
        {
            this.playerCount = playerCount;
            this.highMarble = highMarble;

            currentMarble = new Marble(0);
            currentMarble.cw = currentMarble;
            currentMarble.ccw = currentMarble;
        }

        public void Simulate()
        {
            while (nextMarble <= highMarble)
            {
                if (nextMarble % 23 != 0)
                {
                    DoNormalTurn();
                }
                else
                {
                    DoCrazyRuleTurn();
                }

                nextMarble++;
                currentPlayer = currentPlayer % playerCount + 1;
            }
        }

        private void DoNormalTurn()
        {
            // move cw one, place next marble cw.
            currentMarble = currentMarble.cw;

            var currentCw = currentMarble.cw;

            var newMarble = new Marble(nextMarble);
            currentMarble.cw = newMarble;
            newMarble.ccw = currentMarble;
            newMarble.cw = currentCw;
            currentCw.ccw = newMarble;

            currentMarble = newMarble;
        }

        public void DoCrazyRuleTurn()
        {
            Scores.TryAdd(currentPlayer, 0);
            Scores[currentPlayer] += nextMarble;

            // go ccw 7 marbles, add points to player, remove marble
            for (var i = 0; i < 7; i++)
            {
                currentMarble = currentMarble.ccw;
            }

            Scores[currentPlayer] += currentMarble.value;

            currentMarble.ccw.cw = currentMarble.cw;
            currentMarble.cw.ccw = currentMarble.ccw;
            currentMarble = currentMarble.cw;
        }

        public long GetHighScore()
        {
            return Scores.Select(x => x.Value).Max();
        }

        public void Print()
        {
            Console.Write($" [{currentMarble.value}] ");

            var toPrint = currentMarble.cw;

            while (toPrint != currentMarble)
            {
                Console.Write($" {toPrint.value} ");
                toPrint = toPrint.cw;
            }

            Console.WriteLine();
        }
    }

    public class Marble
    {
        public int value;

        public Marble cw;
        public Marble ccw;

        public Marble(int value)
        {
            this.value = value;
        }
    }
}