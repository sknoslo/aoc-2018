﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace DayNine
{
    class Program
    {
        static void Main(string[] args)
        {
            var rulesRegex = new Regex(@"^(\d+)\splayers.*worth\s(\d+)\spoints$");

            var input = File.ReadAllText("input.txt").Trim();

            var match = rulesRegex.Match(input);

            var numPlayers = int.Parse(match.Groups[1].Value);
            var highMarble = int.Parse(match.Groups[2].Value);

            Console.WriteLine("##### Day Nine #####");

            Console.WriteLine($"Part 1: {PartOne.Run(numPlayers, highMarble)}");
            // Console.WriteLine($"Part 2: ${PartTwo.Run(input)}");
        }
    }
}
