using System;
using System.Text.RegularExpressions;

namespace DayTen
{
    public class Light
    {
        private static readonly Regex regex = new Regex(@"<\s*(-?\d+),\s*(-?\d+)>.*<\s*(-?\d+),\s*(-?\d+)>");

        public int x;
        public int y;
        public int vx;
        public int vy;

        public void Simulate(int seconds)
        {
            x += vx * seconds;
            y += vy * seconds;
        }

        public static Light Parse(string input)
        {
            var match = regex.Match(input);

            return new Light
            {
                x = int.Parse(match.Groups[1].Value),
                y = int.Parse(match.Groups[2].Value),
                vx = int.Parse(match.Groups[3].Value),
                vy = int.Parse(match.Groups[4].Value),
            };
        }
    }
}