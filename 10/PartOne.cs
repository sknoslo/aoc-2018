using System;
using System.Linq;

namespace DayTen
{
    public static class PartOne
    {
        public static void Run(string[] input)
        {
            var lights = input.Select(Light.Parse).ToList();

            var timeElapsed = 0;
            var boundsShrinking = true;
            var previousSize = lights.Max(light => light.x) - lights.Min(light => light.x);

            while (boundsShrinking)
            {
                lights.ForEach(light => light.Simulate(1));

                var currentSize = lights.Max(light => light.x) - lights.Min(light => light.x);

                if (currentSize > previousSize) boundsShrinking = false;

                previousSize = currentSize;
                timeElapsed++;
            }
            timeElapsed--;

            lights.ForEach(light => light.Simulate(-1));

            var xMin = lights.Min(light => light.x);
            var xMax = lights.Max(light => light.x);
            var yMin = lights.Min(light => light.y);
            var yMax = lights.Max(light => light.y);
            var width = xMax - xMin + 1;
            var height = yMax - yMin + 1;

            var message = new char[width, height];

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    message[x, y] = '.';
                }
            }

            foreach (var light in lights)
            {
                message[light.x - xMin, light.y - yMin] = '#';
            }

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    Console.Write(message[x, y]);
                }
                Console.WriteLine();
            }

            Console.WriteLine($"Appeared after {timeElapsed} seconds");
        }
    }
}