﻿using System;
using System.IO;

namespace DayTen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("##### Day Ten 2018 #####");

            var input = File.ReadAllLines("input.txt");

            PartOne.Run(input);
        }
    }
}
