using System;

namespace DayEleven
{
    public struct FuelCell
    {
        public int powerLevel;

        public FuelCell(int x, int y, int serialNumber)
        {
            var rackId = x + 10;
            powerLevel = rackId * y;
            powerLevel += serialNumber;
            powerLevel *= rackId;
            powerLevel = powerLevel / 100 % 10;
            powerLevel -= 5;
        }
    }
}