using System;

namespace DayEleven
{
    public static class PartOne
    {
        public static string Run(FuelCell[,] fuelCells)
        {
            var highestLevel = 0;
            var highestCoords = "X,Y";
            for (var x = 0; x < 300 - 3; ++x)
            {
                for (var y = 0; y < 300 - 3; ++y)
                {
                    var threeByThreeLevel = 0;
                    for (var i = 0; i < 3; ++i)
                    {
                        for (var j = 0; j < 3; ++j)
                        {
                            threeByThreeLevel += fuelCells[x + i, y + j].powerLevel;
                        }
                    }

                    if (threeByThreeLevel > highestLevel)
                    {
                        highestLevel = threeByThreeLevel;
                        highestCoords = $"{x + 1},{y + 1}";
                    }
                }
            }

            return highestCoords;
        }
    }
}