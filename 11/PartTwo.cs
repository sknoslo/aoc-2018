using System;
using System.Linq;

namespace DayEleven
{
    public static class PartTwo
    {
        public static string Run(FuelCell[,] fuelCells)
        {
            var highestLevel = 0;
            var highestCoords = "X,Y";
            for (var x = 0; x < 300 - 3; ++x)
            {
                for (var y = 0; y < 300 - 3; ++y)
                {
                    var maxSize = Math.Min(300 - x, 300 - y);
                    var prevGroupLevel = 0;
                    for (var size = 1; size <= maxSize; ++size)
                    {
                        var groupLevel = prevGroupLevel;
                        for (var i = 0; i < size; ++i)
                        {
                            groupLevel += fuelCells[x + i, y + size - 1].powerLevel;
                            // don't double count bottom right corner
                            if (i < size - 1) groupLevel += fuelCells[x + size - 1, y + i].powerLevel;
                        }

                        if (groupLevel > highestLevel)
                        {
                            highestLevel = groupLevel;
                            highestCoords = $"{x + 1},{y + 1},{size}";
                        }

                        prevGroupLevel = groupLevel;
                    }
                }
            }

            return highestCoords;
        }
    }
}