﻿using System;

namespace DayEleven
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = 9995;
            var fuelCells = new FuelCell[300, 300];

            for (var x = 1; x <= 300; x++)
            {
                for (var y = 1; y <= 300; y++)
                {
                    fuelCells[x - 1, y - 1] = new FuelCell(x, y, input);
                }
            }

            Console.WriteLine("##### Day Eleven 2018 #####");
            Console.WriteLine($"Part 1: {PartOne.Run(fuelCells)}");
            Console.WriteLine($"Part 2: {PartTwo.Run(fuelCells)}");
        }
    }
}
