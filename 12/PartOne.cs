using System;
using System.Collections.Generic;

namespace DayTwelve
{
    public static class PartOne
    {
        public static int Run(Pot potZero, Dictionary<string, Rule> rules)
        {
            var currentPot = potZero;
            for (long generation = 0; generation < 20; ++generation)
            {
                currentPot = currentPot.Pad();
                var nextGen = new Pot(currentPot.label, false);
                do
                {
                    var rule = rules[currentPot.GroupString()];
                    nextGen.hasPlant = rule.producesPlant;
                    nextGen = nextGen.InsertRight(new Pot(currentPot.label + 1, false));
                    currentPot = currentPot.right;
                } while (currentPot != null);

                currentPot = nextGen;
            }

            currentPot = currentPot.TraverseLeft();
            var sum = 0;
            do
            {
                sum += currentPot.hasPlant ? currentPot.label : 0;
                currentPot = currentPot.right;
            } while (currentPot != null);

            return sum;
        }
    }
}