using System;
using System.Collections.Generic;

namespace DayTwelve
{
    public static class PartTwo
    {
        public static ulong Run(ulong gens)
        {
            // using part 1, discovered that the pattern starts to repeat sometime between 100-200 gens
            // just shifted one pot to the right. That pattern is solved by 102 * gens + 1377.
            return 102 * gens + 1377;
        }
    }
}