using System;

namespace DayTwelve
{
    public class Pot
    {
        public int label;
        public bool hasPlant;
        public Pot left;
        public Pot right;

        public Pot(int label, bool hasPlant)
        {
            this.label = label;
            this.hasPlant = hasPlant;
        }

        public string GroupString()
        {
            var l2 = (left?.left?.hasPlant ?? false) ? '#' : '.';
            var l1 = (left?.hasPlant ?? false) ? '#' : '.';
            var c = hasPlant ? '#' : '.';
            var r1 = (right?.hasPlant ?? false) ? '#' : '.';
            var r2 = (right?.right?.hasPlant ?? false) ? '#' : '.';

            return $"{l2}{l1}{c}{r1}{r2}";
        }

        public Pot InsertLeft(Pot pot)
        {
            pot.left = left;
            pot.right = this;
            if (left != null) left.right = pot;
            left = pot;

            return pot;
        }

        public Pot InsertRight(Pot pot)
        {
            pot.right = right;
            pot.left = this;
            if (right != null) right.left = pot;
            right = pot;

            return pot;
        }

        public Pot TraverseLeft()
        {
            var pot = this;
            while (pot.left != null)
            {
                pot = pot.left;
            }

            return pot;
        }

        public Pot TraverseRight()
        {
            var pot = this;
            while (pot.right != null)
            {
                pot = pot.right;
            }

            return pot;
        }

        // Insert 2 pots at each end and return the new left-most pot
        public Pot Pad()
        {
            var pot = TraverseRight();
            if (pot.left.hasPlant || pot.hasPlant) pot.InsertRight(new Pot(pot.label + 1, false));
            if (pot.hasPlant) pot.right.InsertRight(new Pot(pot.label + 2, false));
            pot = TraverseLeft();
            if (pot.right.hasPlant || pot.hasPlant) pot.InsertLeft(new Pot(pot.label - 1, false));
            if (pot.hasPlant) pot.left.InsertLeft(new Pot(pot.label - 2, false));
            return TraverseLeft();
        }

        public override string ToString()
        {
            var pot = TraverseLeft();
            var result = "";

            do
            {
                result += pot.hasPlant ? '#' : '.';
                pot = pot.right;
            } while (pot != null);

            return result;
        }
    }
}