﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DayTwelve
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines("input.txt");

            var currentLabel = 0;
            var pot = input[0].Replace("initial state: ", "").Aggregate(
                null,
                (Pot prev, char c) =>
                {
                    var p = new Pot(currentLabel++, c == '#');
                    if (prev == null) return p;

                    return prev.InsertRight(p);
                },
                (Pot p) => p.TraverseLeft());

            var rules = input.Skip(2).Select(Rule.Parse).ToDictionary(r => r.pattern, r => r);

            Console.WriteLine("##### Day Twelve #####");
            Console.WriteLine($"Part 1: {PartOne.Run(pot, rules)}");
            Console.WriteLine($"Part 2: {PartTwo.Run(50000000000)}");
        }
    }
}
