using System.Text.RegularExpressions;

namespace DayTwelve
{
    public struct Rule
    {
        private static Regex regex = new Regex(@"^([.#]{5}).*([.#])$");
        public string pattern;
        public bool producesPlant;

        public static Rule Parse(string input)
        {
            var matches = regex.Match(input);

            return new Rule
            {
                pattern = matches.Groups[1].Value,
                producesPlant = matches.Groups[2].Value == "#"
            };
        }

        public override string ToString()
        {
            var result = producesPlant ? '#' : '.';

            return $"{pattern} => {result}";
        }
    }
}