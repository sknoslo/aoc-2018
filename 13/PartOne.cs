using System;
using System.Collections.Generic;
using System.Linq;

namespace DayThirteen
{
    public static class PartOne
    {
        public static void Run(char[,] track, List<Cart> carts)
        {
            var crash = false;

            while (!crash)
            {
                carts.Sort((a, b) =>
                {
                    if (a.y < b.y && (a.y == b.y && a.x < b.x)) return -1;
                    return 1;
                });

                foreach (var cart in carts)
                {
                    crash = Step(track, cart, carts);
                    if (crash) break;
                }
            }
        }

        public static bool Step(char[,] track, Cart cart, List<Cart> carts)
        {
            // update location
            switch (cart.dir)
            {
                case 'v':
                    ++cart.y;
                    break;
                case '^':
                    --cart.y;
                    break;
                case '>':
                    ++cart.x;
                    break;
                case '<':
                    --cart.x;
                    break;
            }

            // check collisions
            foreach (var crt in carts)
            {
                if (crt == cart) continue;

                if (crt.x == cart.x && crt.y == cart.y)
                {
                    track[cart.x, cart.y] = 'X';

                    Console.WriteLine($"Crash at {cart.x},{cart.y}");
                    return true;
                }
            }

            // update cart direction
            switch (track[cart.x, cart.y])
            {
                case '+':
                    cart.HandleIntersection();
                    break;
                case '\\':
                    if (cart.dir == '<' || cart.dir == '>') cart.TurnRight();
                    else cart.TurnLeft();
                    break;
                case '/':
                    if (cart.dir == 'v' || cart.dir == '^') cart.TurnRight();
                    else cart.TurnLeft();
                    break;
            }

            return false;
        }
    }
}
