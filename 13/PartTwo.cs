using System;
using System.Collections.Generic;
using System.Linq;

namespace DayThirteen
{
    public static class PartTwo
    {
        public static void Run(char[,] track, List<Cart> carts)
        {
            var onlyOne = false;
            while (!onlyOne)
            {
                carts.Sort((a, b) =>
                {
                    if (a.y < b.y || (a.y == b.y && a.x < b.x)) return -1;
                    return 1;
                });

                foreach (var cart in carts)
                {
                    if (cart.crashed) continue;
                    onlyOne = Step(track, cart, carts) || onlyOne;
                }
            }

            Print(track, carts);
            var winner = carts.First(c => !c.crashed);

            Console.WriteLine($"Last cart at {winner.x},{winner.y}");
        }

        public static bool Step(char[,] track, Cart cart, List<Cart> carts)
        {
            // update location
            switch (cart.dir)
            {
                case 'v':
                    ++cart.y;
                    break;
                case '^':
                    --cart.y;
                    break;
                case '>':
                    ++cart.x;
                    break;
                case '<':
                    --cart.x;
                    break;
            }

            // check collisions
            foreach (var crt in carts)
            {
                if (crt == cart || crt.crashed) continue;

                if (crt.x == cart.x && crt.y == cart.y)
                {
                    cart.crashed = true;
                    crt.crashed = true;

                    if (carts.Count(c => !c.crashed) < 2) return true;
                }
            }

            // update cart direction
            switch (track[cart.x, cart.y])
            {
                case '+':
                    cart.HandleIntersection();
                    break;
                case '\\':
                    if (cart.dir == '<' || cart.dir == '>') cart.TurnRight();
                    else cart.TurnLeft();
                    break;
                case '/':
                    if (cart.dir == 'v' || cart.dir == '^') cart.TurnRight();
                    else cart.TurnLeft();
                    break;
            }

            return false;
        }

        public static void Print(char[,] track, List<Cart> carts)
        {
            for (var y = 0; y < track.GetLength(1); ++y)
            {
                var line = "";
                for (var x = 0; x < track.GetLength(0); ++x)
                {
                    var cart = carts.Where(c => c.x == x && c.y == y).FirstOrDefault();
                    if (cart != null && !cart.crashed)
                    {
                        line += cart.dir;
                    }
                    else if (cart != null && cart.crashed)
                    {
                        line += 'X';
                    }
                    else
                    {
                        line += track[x, y];
                    }
                }
                Console.WriteLine(line);
            }
            Console.WriteLine();
        }
    }
}
