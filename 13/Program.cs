﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DayThirteen
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines("input.txt");

            var h = input.Length;
            var w = input[0].Length;

            var track = new char[w, h];
            var carts = new List<Cart>();

            var y = 0;
            foreach (var line in input)
            {
                var x = 0;
                foreach (var c in line)
                {
                    switch (c)
                    {
                        case '<':
                        case '>':
                            carts.Add(new Cart { x = x, y = y, dir = c });
                            track[x, y] = '-';
                            break;
                        case 'v':
                        case '^':
                            carts.Add(new Cart { x = x, y = y, dir = c });
                            track[x, y] = '|';
                            break;
                        default:
                            track[x, y] = c;
                            break;
                    }
                    ++x;
                }
                ++y;
            }

            // PartOne.Run(track, carts);
            PartTwo.Run(track, carts);
        }
    }

    public class Cart
    {
        public int x, y, crossings = 0;
        public char dir;
        public bool crashed = false;

        public void HandleIntersection()
        {
            switch (crossings++ % 3)
            {
                case 0:
                    TurnLeft();
                    break;
                case 1:
                    break; // go straight
                case 2:
                    TurnRight();
                    break;
            }
        }

        public void TurnLeft()
        {
            switch (dir)
            {
                case '^':
                    dir = '<';
                    break;
                case '<':
                    dir = 'v';
                    break;
                case 'v':
                    dir = '>';
                    break;
                case '>':
                    dir = '^';
                    break;
            }
        }

        public void TurnRight()
        {
            switch (dir)
            {
                case '^':
                    dir = '>';
                    break;
                case '<':
                    dir = '^';
                    break;
                case 'v':
                    dir = '<';
                    break;
                case '>':
                    dir = 'v';
                    break;
            }
        }
    }
}
