fn main() {
    let d1_input = 290431;
    let d2_input = &[2, 9, 0, 4, 3, 1];

    println!("##### Day Fourteen 2018 #####");

    do_part_one(d1_input);
    do_part_two(d2_input);
}

fn do_part_one(input: usize) {
    let mut recipes = vec![3, 7];
    let target_len = input + 10;

    let mut elfa = 0;
    let mut elfb = 1;

    while recipes.len() < target_len {
        let new_recipe = recipes[elfa] + recipes[elfb];
        if new_recipe > 9 {
            recipes.push(new_recipe / 10);
            recipes.push(new_recipe % 10);
        } else {
            recipes.push(new_recipe);
        }

        elfa = (elfa + recipes[elfa] as usize + 1) % recipes.len();
        elfb = (elfb + recipes[elfb] as usize + 1) % recipes.len();
    }

    let result = recipes[target_len - 10..target_len]
        .iter()
        .map(|a| a.to_string())
        .collect::<String>();

    println!("Part 1: {}", result);
}

fn do_part_two(input: &[u8]) {
    let len = input.len();
    let mut recipes: Vec<u8> = vec![3, 7];

    let mut elfa = 0;
    let mut elfb = 1;

    loop {
        let new_recipe = recipes[elfa] + recipes[elfb];
        if new_recipe > 9 {
            recipes.push(new_recipe / 10);

            if is_match(&recipes, &input) {
                break;
            }

            recipes.push(new_recipe % 10);
        } else {
            recipes.push(new_recipe);
        }

        if is_match(&recipes, &input) {
            break;
        }

        elfa = (elfa + recipes[elfa] as usize + 1) % recipes.len();
        elfb = (elfb + recipes[elfb] as usize + 1) % recipes.len();
    }

    let result = recipes.len() - len;
    println!("Part 2: {}", result);
}

fn is_match(recipes: &Vec<u8>, target: &[u8]) -> bool {
    recipes.len() > target.len() && &recipes[recipes.len() - target.len()..] == target
}
