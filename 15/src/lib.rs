use std::collections::{HashMap, VecDeque};

const DIRS: [(i32, i32); 4] = [(0, -1), (-1, 0), (1, 0), (0, 1)];

#[derive(Debug)]
pub struct Game {
    pub w: usize,
    pub h: usize,
    pub rem_elves: u32,
    pub rem_goblins: u32,
    pub map: Vec<Vec<Tile>>,
    pub round: u32,
    pub elf_ap: i32,
    pub gob_ap: i32,
}

#[derive(Debug, Copy, Clone)]
pub enum Tile {
    Wall,
    Goblin { hp: i32, rounds: u32 },
    Elf { hp: i32, rounds: u32 },
    Nothing,
}

pub enum MoveResult {
    MoveTo(usize, usize),
    Target(usize, usize),
}

pub enum SimResult {
    GameOver,
    Continue,
}

impl Game {
    pub fn new(input: &Vec<Vec<char>>) -> Game {
        let mut rem_elves = 0;
        let mut rem_goblins = 0;

        let map = input
            .iter()
            .map(|row| {
                row.iter()
                    .map(|cell| match cell {
                        '.' => Tile::Nothing,
                        '#' => Tile::Wall,
                        'E' => {
                            rem_elves += 1;
                            Tile::Elf { hp: 200, rounds: 0 }
                        }
                        'G' => {
                            rem_goblins += 1;
                            Tile::Goblin { hp: 200, rounds: 0 }
                        }
                        _ => panic!("bad input"),
                    })
                    .collect()
            })
            .collect();

        Game {
            w: input[0].len(),
            h: input.len(),
            rem_elves,
            rem_goblins,
            map,
            round: 0,
            elf_ap: 3,
            gob_ap: 3,
        }
    }

    pub fn sim_round(&mut self) -> SimResult {
        for y in 0..self.h {
            for x in 0..self.w {
                match self.map[y][x] {
                    Tile::Nothing | Tile::Wall => continue,
                    _ => {
                        if !self.step_unit(x, y) {
                            return SimResult::GameOver;
                        }
                    }
                }
            }
        }

        self.round += 1;
        SimResult::Continue
    }

    pub fn step_unit(&mut self, x: usize, y: usize) -> bool {
        match self.map[y][x] {
            Tile::Goblin { rounds, hp } => {
                if rounds > self.round {
                    // already took turn, must have moved here
                    return true;
                }

                if self.rem_elves == 0 {
                    return false;
                }

                let (nx, ny) = self.try_move(x, y, true);
                self.map[y][x] = Tile::Nothing;
                self.map[ny][nx] = Tile::Goblin {
                    hp,
                    rounds: rounds + 1,
                };
                self.try_attack(nx, ny, true);
            }
            Tile::Elf { rounds, hp } => {
                if rounds > self.round {
                    // already took turn, must have moved here
                    return true;
                }

                if self.rem_goblins == 0 {
                    return false;
                }

                let (nx, ny) = self.try_move(x, y, false);
                self.map[y][x] = Tile::Nothing;
                self.map[ny][nx] = Tile::Elf {
                    hp,
                    rounds: rounds + 1,
                };
                self.try_attack(nx, ny, false);
            }
            _ => panic!("how did this get called?"),
        }

        true
    }

    fn try_attack(&mut self, x: usize, y: usize, is_goblin: bool) {
        let mut target_hp = None;
        let mut target_coords = None;

        for (dx, dy) in DIRS.iter() {
            let nx = (x as i32 + dx) as usize;
            let ny = (y as i32 + dy) as usize;
            match self.map[ny][nx] {
                Tile::Goblin { hp, .. } => {
                    if !is_goblin {
                        match target_hp {
                            Some(h) => {
                                if hp < h {
                                    target_hp = Some(hp);
                                    target_coords = Some((nx, ny));
                                }
                            }
                            None => {
                                target_hp = Some(hp);
                                target_coords = Some((nx, ny));
                            }
                        }
                    }
                }
                Tile::Elf { hp, .. } => {
                    if is_goblin {
                        match target_hp {
                            Some(h) => {
                                if hp < h {
                                    target_hp = Some(hp);
                                    target_coords = Some((nx, ny));
                                }
                            }
                            None => {
                                target_hp = Some(hp);
                                target_coords = Some((nx, ny));
                            }
                        }
                    }
                }
                _ => {} // keep looking
            }
        }

        match target_coords {
            Some((tx, ty)) => {
                self.attack(tx, ty);
            }
            None => {} // no one to attack
        }
    }

    fn try_move(&mut self, x: usize, y: usize, is_goblin: bool) -> (usize, usize) {
        for (dx, dy) in DIRS.iter() {
            let nx = (x as i32 + dx) as usize;
            let ny = (y as i32 + dy) as usize;
            match self.map[ny][nx] {
                Tile::Goblin { .. } => {
                    if !is_goblin {
                        return (x, y); // don't move necessary?
                    }
                }
                Tile::Elf { .. } => {
                    if is_goblin {
                        return (x, y); // don't move necessary?
                    }
                }
                _ => {} // keep looking
            }
        }

        let mut queue = VecDeque::new();
        let mut visited = HashMap::new();
        queue.push_front((x, y, None));
        let mut first_pass = true;
        while !queue.is_empty() {
            let mut target_found = None;
            for _ in 0..queue.len() {
                let (vx, vy, vfrom) = queue.pop_back().unwrap();

                if visited.contains_key(&(vx, vy)) {
                    continue;
                }

                visited.insert((vx, vy), vfrom);

                for (dx, dy) in DIRS.iter() {
                    let nx = (vx as i32 + dx) as usize;
                    let ny = (vy as i32 + dy) as usize;

                    if visited.contains_key(&(nx, ny)) {
                        continue;
                    }

                    match self.map[ny][nx] {
                        Tile::Nothing => {
                            if first_pass {
                                // so it doesn't walk back to the starting point... kinda hacky. don't care.
                                queue.push_front((nx, ny, None));
                            } else {
                                queue.push_front((nx, ny, Some((vx, vy))));
                            }
                        }
                        Tile::Goblin { .. } => {
                            if !is_goblin {
                                // println!("[Elf] found a path, target is {}, {}", vx, vy);
                                match target_found {
                                    None => target_found = Some((vx, vy)),
                                    Some((ox, oy)) => {
                                        if vy < oy || (vy == oy && vx < ox) {
                                            // multiple targets found in distance
                                            // take he first in reading order
                                            target_found = Some((vx, vy));
                                        }
                                    }
                                }
                            }
                        }
                        Tile::Elf { .. } => {
                            if is_goblin {
                                // println!("[Goblin] found a path, target is {}, {}", vx, vy);
                                match target_found {
                                    None => target_found = Some((vx, vy)),
                                    Some((ox, oy)) => {
                                        if vy < oy || (vy == oy && vx < ox) {
                                            // multiple targets found in distance
                                            // take he first in reading order
                                            target_found = Some((vx, vy));
                                        }
                                    }
                                }
                            }
                        }
                        _ => {} // path blocked
                    }
                }
            }

            match target_found {
                Some((tx, ty)) => return self.walk_back(&visited, tx, ty),
                None => {}
            }

            first_pass = false;
        }

        (x, y) // didn't find a place to go
    }

    fn walk_back(
        &self,
        visited: &HashMap<(usize, usize), Option<(usize, usize)>>,
        mut x: usize,
        mut y: usize,
    ) -> (usize, usize) {
        while let Some(Some((fx, fy))) = visited.get(&(x, y)) {
            // println!("walking back f {}, {}", fx, fy);
            x = *fx;
            y = *fy;
        }

        (x, y)
    }

    fn attack(&mut self, x: usize, y: usize) {
        match self.map[y][x] {
            Tile::Goblin { hp, rounds } => {
                let nhp = hp - self.elf_ap;
                if nhp <= 0 {
                    self.rem_goblins -= 1;
                    self.map[y][x] = Tile::Nothing;
                } else {
                    self.map[y][x] = Tile::Goblin { hp: nhp, rounds };
                }
            }
            Tile::Elf { hp, rounds } => {
                let nhp = hp - self.gob_ap;
                if nhp <= 0 {
                    self.rem_elves -= 1;
                    self.map[y][x] = Tile::Nothing;
                } else {
                    self.map[y][x] = Tile::Elf { hp: nhp, rounds };
                }
            }
            _ => panic!("quit hitting walls"),
        }
    }

    pub fn calc_score(&self) -> u32 {
        let mut score = 0;

        for y in 0..self.h {
            for x in 0..self.w {
                score += match self.map[y][x] {
                    Tile::Goblin { hp, .. } | Tile::Elf { hp, .. } => hp,
                    _ => 0,
                }
            }
        }

        score as u32 * self.round
    }

    pub fn to_string(&self) -> String {
        let mut print = String::new();
        print.push_str("Round: ");
        print.push_str(&self.round.to_string());
        print.push_str("\n");

        let map = self
            .map
            .iter()
            .map(|row| {
                let mut to_append = String::new();
                let mut print_row = row
                    .iter()
                    .map(|cell| match cell {
                        Tile::Nothing => '.',
                        Tile::Wall => '#',
                        Tile::Elf { hp, .. } => {
                            to_append.push_str(&format!(" E({})", hp));

                            'E'
                        }
                        Tile::Goblin { hp, .. } => {
                            to_append.push_str(&format!(" G({})", hp));

                            'G'
                        }
                    })
                    .collect::<String>();
                print_row.push_str(&to_append);
                print_row
            })
            .collect::<Vec<String>>()
            .join("\n");

        print.push_str(&map);
        print.push_str("\n");

        print
    }
}
