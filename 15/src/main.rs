use d1518::{Game, SimResult};
use std::fs;

fn main() {
    let map = fs::read_to_string("input.txt").unwrap();

    let input: Vec<Vec<char>> = map
        .lines()
        .map(|line| line.chars().collect::<Vec<char>>())
        .collect();

    let mut game = Game::new(&input);

    do_part_one(&mut game);
    println!("Part 1: {}", game.calc_score());
    println!("Part 2: {}", do_part_two(&input));
}

fn do_part_one(game: &mut Game) {
    loop {
        match game.sim_round() {
            SimResult::Continue => {
                // println!("{}", game.to_string());
            }
            SimResult::GameOver => break,
        }
    }

    // println!("{}", game.to_string());
}

fn do_part_two(input: &Vec<Vec<char>>) -> u32 {
    let mut elf_ap = 4;
    loop {
        let mut game = Game::new(&input);
        game.elf_ap = elf_ap;
        let elf_count = game.rem_elves;
        loop {
            match game.sim_round() {
                SimResult::Continue => {
                    if game.rem_elves < elf_count {
                        println!("{} -> ending game early, an elf died", elf_ap);
                        break;
                    }
                }
                SimResult::GameOver => {
                    println!("[{}], {}", game.elf_ap, game.round);
                    return game.calc_score();
                }
            }
        }
        elf_ap += 1;
    }
}
