#[derive(Debug)]
pub struct Sample {
    pub before: [usize; 4],
    pub op: usize,
    pub a: usize,
    pub b: usize,
    pub c: usize,
    pub after: [usize; 4],
}

#[derive(Debug)]
pub struct Watch {
    pub rg: [usize; 4],
}

pub fn get_op_code(num: usize) -> &'static str {
    match num {
        0 => "borr",
        1 => "seti",
        2 => "mulr",
        3 => "eqri",
        4 => "banr",
        5 => "bori",
        6 => "bani",
        7 => "gtri",
        8 => "addr",
        9 => "muli",
        10 => "addi",
        11 => "eqrr",
        12 => "gtir",
        13 => "eqir",
        14 => "setr",
        15 => "gtrr",
        _ => panic!("bad op code"),
    }
}

impl Watch {
    pub fn new() -> Watch {
        Watch { rg: [0, 0, 0, 0] }
    }

    pub fn set(&mut self, state: &[usize; 4]) {
        self.rg[0] = state[0];
        self.rg[1] = state[1];
        self.rg[2] = state[2];
        self.rg[3] = state[3];
    }

    pub fn proc(&mut self, op: &str, ia: usize, ib: usize, o: usize) {
        match op {
            "addr" => self.rg[o] = self.rg[ia] + self.rg[ib],
            "addi" => self.rg[o] = self.rg[ia] + ib,
            "mulr" => self.rg[o] = self.rg[ia] * self.rg[ib],
            "muli" => self.rg[o] = self.rg[ia] * ib,
            "banr" => self.rg[o] = self.rg[ia] & self.rg[ib],
            "bani" => self.rg[o] = self.rg[ia] & ib,
            "borr" => self.rg[o] = self.rg[ia] | self.rg[ib],
            "bori" => self.rg[o] = self.rg[ia] | ib,
            "setr" => self.rg[o] = self.rg[ia],
            "seti" => self.rg[o] = ia,
            "gtir" => self.rg[o] = if ia > self.rg[ib] { 1 } else { 0 },
            "gtri" => self.rg[o] = if self.rg[ia] > ib { 1 } else { 0 },
            "gtrr" => self.rg[o] = if self.rg[ia] > self.rg[ib] { 1 } else { 0 },
            "eqir" => self.rg[o] = if ia == self.rg[ib] { 1 } else { 0 },
            "eqri" => self.rg[o] = if self.rg[ia] == ib { 1 } else { 0 },
            "eqrr" => self.rg[o] = if self.rg[ia] == self.rg[ib] { 1 } else { 0 },
            _ => panic!("bad op code!"),
        }
    }

    // pub fn sample(&self, ia: usize, ib: i32, o: usize) -> String {
    //     let mut possible_count = 0;

    //     for//

    //     possible_count
    // }
}
