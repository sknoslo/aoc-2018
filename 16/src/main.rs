use d1618::{get_op_code, Sample, Watch};
use regex::Regex;
use std::fs;

fn main() {
    println!("Part 1: {}", do_part_one());
    println!("Part 2: {}", do_part_two());
}

fn do_part_one() -> usize {
    let reg_re = Regex::new(r".*\[(\d+),\s(\d+),\s(\d+),\s(\d+)\]").unwrap();
    let inst_re = Regex::new(r"(\d+)\s(\d+)\s(\d+)\s(\d+)").unwrap();

    let input = fs::read_to_string("input_p1.txt").unwrap();

    let samples: Vec<Sample> = input
        .split("\n\n")
        .map(|a| {
            let sample = a.lines().collect::<Vec<&str>>();
            let before = reg_re.captures(sample[0]).unwrap();
            let inst = inst_re.captures(sample[1]).unwrap();
            let after = reg_re.captures(sample[2]).unwrap();

            Sample {
                before: [
                    before[1].parse().unwrap(),
                    before[2].parse().unwrap(),
                    before[3].parse().unwrap(),
                    before[4].parse().unwrap(),
                ],
                op: inst[1].parse().unwrap(),
                a: inst[2].parse().unwrap(),
                b: inst[3].parse().unwrap(),
                c: inst[4].parse().unwrap(),
                after: [
                    after[1].parse().unwrap(),
                    after[2].parse().unwrap(),
                    after[3].parse().unwrap(),
                    after[4].parse().unwrap(),
                ],
            }
        })
        .collect();

    let mut watch = Watch::new();
    let mut count = 0;

    for sample in samples.iter() {
        let mut match_count = 0;
        for op in 0..16 {
            let op_code = get_op_code(op);
            watch.set(&sample.before);
            watch.proc(op_code, sample.a, sample.b, sample.c);
            if watch.rg == sample.after {
                match_count += 1;
            }
        }

        if match_count >= 3 {
            count += 1;
        }
    }

    count
}

fn do_part_two() -> usize {
    let mut watch = Watch::new();

    let program = fs::read_to_string("input_p2.txt").unwrap();

    for line in program.lines() {
        let instruction = line
            .split_whitespace()
            .map(|a| a.parse().unwrap())
            .collect::<Vec<usize>>();

        watch.proc(
            get_op_code(instruction[0]),
            instruction[1],
            instruction[2],
            instruction[3],
        );
    }

    watch.rg[0]
}
