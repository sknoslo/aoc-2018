use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{HashMap, VecDeque};
use std::fs;

fn main() {
    let file = fs::read_to_string("input.txt").unwrap();
    let instructions = file
        .lines()
        .map(|line| line.parse::<Instruction>().unwrap())
        .collect::<Vec<_>>();
    let minx = instructions.iter().map(|i| i.start.x).min().unwrap() - 1;
    let maxx = instructions.iter().map(|i| i.end.x).max().unwrap() + 1;
    let miny = instructions.iter().map(|i| i.start.y).min().unwrap();
    let maxy = instructions.iter().map(|i| i.end.y).max().unwrap();

    let mut map: HashMap<(i32, i32), Tile> = HashMap::new();
    map.insert((500, 0), Tile::Source);

    for i in instructions.iter() {
        for x in i.start.x..i.end.x + 1 {
            for y in i.start.y..i.end.y + 1 {
                map.insert((x, y), Tile::Clay);
            }
        }
    }

    simulate(&mut map, minx, miny, maxx, maxy);

    let part_one = map
        .iter()
        .filter(|((_, y), v)| *y >= miny && (**v == Tile::Stream || **v == Tile::Water))
        .count();
    let part_two = map.iter().filter(|(_, v)| **v == Tile::Water).count();

    println!("Part 1: {}", part_one);
    println!("Part 2: {}", part_two);
}

fn simulate(mut map: &mut HashMap<(i32, i32), Tile>, minx: i32, miny: i32, maxx: i32, maxy: i32) {
    let mut queue = VecDeque::new();
    queue.push_front((500, 1));

    while !queue.is_empty() {
        for _ in 0..queue.len() {
            let (x, y) = queue.pop_back().unwrap();

            if y <= maxy {
                map.insert((x, y), Tile::Stream);

                match map.get(&(x, y + 1)) {
                    None => {
                        queue.push_front((x, y + 1));
                    }
                    Some(Tile::Clay) | Some(Tile::Water) => match fill(&mut map, x, y) {
                        FillResult::One(ox, oy) => queue.push_front((ox, oy)),
                        FillResult::Two((lx, ly), (rx, ry)) => {
                            queue.push_front((lx, ly));
                            queue.push_front((rx, ry));
                        }
                    },
                    _ => { /* if land in stream, oh well */ }
                }
            }
        }
    }

    print(&map, minx, miny, maxx, maxy);
}

fn fill(map: &mut HashMap<(i32, i32), Tile>, x: i32, mut y: i32) -> FillResult {
    let mut left_exit = None;
    let mut right_exit = None;

    // loop until an exit is found
    loop {
        map.insert((x, y), Tile::Stream);
        // go left
        let mut lx = x;
        loop {
            lx -= 1;
            match map.get(&(lx, y)) {
                None | Some(Tile::Stream) => {
                    map.insert((lx, y), Tile::Stream);
                }
                Some(_) => break,
            }

            match map.get(&(lx, y + 1)) {
                None | Some(Tile::Stream) => {
                    left_exit = Some((lx, y + 1));
                    break;
                }
                Some(_) => {}
            }
        }

        // go right
        let mut rx = x;
        loop {
            rx += 1;
            match map.get(&(rx, y)) {
                None | Some(Tile::Stream) => {
                    map.insert((rx, y), Tile::Stream);
                }
                Some(_) => break,
            }

            match map.get(&(rx, y + 1)) {
                None | Some(Tile::Stream) => {
                    right_exit = Some((rx, y + 1));
                    break;
                }
                Some(_) => {}
            }
        }

        if right_exit.is_none() && left_exit.is_none() {
            for ix in lx + 1..rx {
                map.insert((ix, y), Tile::Water);
            }

            y -= 1;
        } else {
            break;
        }
    }

    match (left_exit, right_exit) {
        (Some((lx, ly)), Some((rx, ry))) => FillResult::Two((lx, ly), (rx, ry)),
        (Some((lx, ly)), None) => FillResult::One(lx, ly),
        (None, Some((rx, ry))) => FillResult::One(rx, ry),
        _ => panic!("then how did you get here!?!?"),
    }
}

enum FillResult {
    One(i32, i32),
    Two((i32, i32), (i32, i32)),
}

struct Point {
    x: i32,
    y: i32,
}

#[derive(Eq, PartialEq)]
enum Tile {
    Source,
    Water,
    Stream,
    Clay,
}

struct Instruction {
    start: Point,
    end: Point,
}

impl std::str::FromStr for Instruction {
    type Err = std::num::ParseIntError; // never going to happen, just shutup rust

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^([xy])=(\d+),\s[xy]=(\d+)\.\.(\d+)$").unwrap();
        }

        let cap = RE.captures(s).unwrap();

        match &cap[1] {
            "x" => {
                let x = cap[2].parse::<i32>().unwrap();
                let y1 = cap[3].parse::<i32>().unwrap();
                let y2 = cap[4].parse::<i32>().unwrap();
                Ok(Instruction {
                    start: Point { x, y: y1 },
                    end: Point { x, y: y2 },
                })
            }
            "y" => {
                let y = cap[2].parse::<i32>().unwrap();
                let x1 = cap[3].parse::<i32>().unwrap();
                let x2 = cap[4].parse::<i32>().unwrap();
                Ok(Instruction {
                    start: Point { x: x1, y },
                    end: Point { x: x2, y },
                })
            }
            _ => panic!("wtf"),
        }
    }
}

fn print(map: &HashMap<(i32, i32), Tile>, minx: i32, _miny: i32, maxx: i32, maxy: i32) {
    for y in 0..maxy + 1 {
        let mut line = String::new();
        for x in minx..maxx + 1 {
            line.push(match map.get(&(x, y)) {
                Some(Tile::Source) => '+',
                Some(Tile::Clay) => '#',
                Some(Tile::Water) => '~',
                Some(Tile::Stream) => '|',
                None => '.',
            });
        }
        println!("{}", line);
    }
}
