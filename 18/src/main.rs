use std::collections::HashSet;
use std::fs;

const W: usize = 50;
const H: usize = 50;

fn main() {
    let input = fs::read_to_string("input.txt").unwrap();

    let mut p1_area = init_map(&input);
    let mut p2_area = init_map(&input);

    println!("Part 1: {}", do_part_one(&mut p1_area));
    println!("Part 2: {}", do_part_two(&mut p2_area));
}

fn init_map(input: &String) -> [[char; W]; H] {
    let mut area = [['.'; W]; H];

    for (y, row) in input.lines().enumerate() {
        for (x, col) in row.chars().enumerate() {
            area[y][x] = col;
        }
    }

    area
}

fn do_part_one(mut area: &mut [[char; W]; H]) -> i32 {
    for _ in 0..10 {
        do_step(&mut area);
    }

    let mut trees = 0;
    let mut lumberyards = 0;

    for y in 0..H {
        for x in 0..W {
            match area[y][x] {
                '|' => trees += 1,
                '#' => lumberyards += 1,
                _ => {}
            }
        }
    }

    trees * lumberyards
}

fn do_part_two(mut area: &mut [[char; W]; H]) -> i32 {
    let mut cycle_offset = 0;
    let mut seen = HashSet::new();
    let mut cycle_start;
    loop {
        do_step(&mut area);
        cycle_offset += 1;
        cycle_start = count(&area);

        // just iterate for a while before looking for a cycle, as there
        // are some early cycles that don't sustain.
        if cycle_offset > 1000 && !seen.insert(cycle_start) {
            break;
        }
    }

    let mut cycle = vec![cycle_start];
    loop {
        do_step(&mut area);
        let count = count(&area);

        if count == cycle_start {
            break;
        } else {
            cycle.push(count);
        }
    }

    cycle[(1000000000 - cycle_offset) % cycle.len()]
}

fn count(area: &[[char; W]; H]) -> i32 {
    let mut trees = 0;
    let mut lumberyards = 0;

    for y in 0..H {
        for x in 0..W {
            match area[y][x] {
                '|' => trees += 1,
                '#' => lumberyards += 1,
                _ => {}
            }
        }
    }

    trees * lumberyards
}

fn do_step(area: &mut [[char; W]; H]) {
    let temp = area.clone();

    for y in 0..H {
        let iy = y as i32;
        for x in 0..W {
            let ix = x as i32;
            let mut trees = 0;
            let mut lumberyards = 0;

            let counts = [
                check_acre(&temp, ix - 1, iy),
                check_acre(&temp, ix - 1, iy - 1),
                check_acre(&temp, ix, iy - 1),
                check_acre(&temp, ix + 1, iy - 1),
                check_acre(&temp, ix + 1, iy),
                check_acre(&temp, ix + 1, iy + 1),
                check_acre(&temp, ix, iy + 1),
                check_acre(&temp, ix - 1, iy + 1),
            ];

            for (nt, nl) in counts.iter() {
                trees += nt;
                lumberyards += nl;
            }

            if temp[y][x] == '.' && trees >= 3 {
                area[y][x] = '|';
            } else if temp[y][x] == '|' && lumberyards >= 3 {
                area[y][x] = '#';
            } else if temp[y][x] == '#' {
                if lumberyards >= 1 && trees >= 1 {
                    area[y][x] = '#';
                } else {
                    area[y][x] = '.';
                }
            }
        }
    }
}

fn check_acre(area: &[[char; W]; H], x: i32, y: i32) -> (usize, usize) {
    if x < 0 || x >= W as i32 {
        (0, 0)
    } else if y < 0 || y >= H as i32 {
        (0, 0)
    } else {
        match area[y as usize][x as usize] {
            '|' => (1, 0),
            '#' => (0, 1),
            _ => (0, 0),
        }
    }
}

// fn print(area: &[[char; W]; H]) {
//     let mut line = String::new();
//     for y in 0..H {
//         for x in 0..W {
//             line.push(area[y][x]);
//         }
//         println!("{}", line);
//         line.clear();
//     }
// }
