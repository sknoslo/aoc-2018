#[derive(Debug)]
pub struct Watch {
    pub ip: usize,
    pub ipr: usize,
    pub rg: [usize; 6],
}

#[derive(Debug)]
pub struct Instruction<'a>(&'a str, usize, usize, usize);

impl<'a> Instruction<'a> {
    pub fn from_str(line: &str) -> Instruction {
        let parts = line.split_whitespace().collect::<Vec<&str>>();

        Instruction(
            parts[0],
            parts[1].parse().unwrap(),
            parts[2].parse().unwrap(),
            parts[3].parse().unwrap(),
        )
    }
}

impl Watch {
    pub fn new() -> Watch {
        Watch {
            ip: 0,
            ipr: 0,
            rg: [0; 6],
        }
    }

    pub fn exec(&mut self, program: &[&str]) {
        self.ipr = program[0].split_whitespace().collect::<Vec<&str>>()[1]
            .parse()
            .unwrap();

        let instructions = program[1..]
            .iter()
            .map(|line| Instruction::from_str(line))
            .collect::<Vec<_>>();

        loop {
            self.rg[self.ipr] = self.ip;
            self.proc(&instructions[self.ip]);
            self.ip = self.rg[self.ipr] + 1;
            if instructions.get(self.ip).is_none() {
                break;
            }
        }
    }

    pub fn proc(&mut self, instruction: &Instruction) {
        let &Instruction(op, ia, ib, o) = instruction;

        match op {
            "modr" => self.rg[o] = self.rg[ia] % self.rg[ib], // to solve part 2, had to add mod opcode to optimize the program
            "addr" => self.rg[o] = self.rg[ia] + self.rg[ib],
            "addi" => self.rg[o] = self.rg[ia] + ib,
            "mulr" => self.rg[o] = self.rg[ia] * self.rg[ib],
            "muli" => self.rg[o] = self.rg[ia] * ib,
            "banr" => self.rg[o] = self.rg[ia] & self.rg[ib],
            "bani" => self.rg[o] = self.rg[ia] & ib,
            "borr" => self.rg[o] = self.rg[ia] | self.rg[ib],
            "bori" => self.rg[o] = self.rg[ia] | ib,
            "setr" => self.rg[o] = self.rg[ia],
            "seti" => self.rg[o] = ia,
            "gtir" => self.rg[o] = if ia > self.rg[ib] { 1 } else { 0 },
            "gtri" => self.rg[o] = if self.rg[ia] > ib { 1 } else { 0 },
            "gtrr" => self.rg[o] = if self.rg[ia] > self.rg[ib] { 1 } else { 0 },
            "eqir" => self.rg[o] = if ia == self.rg[ib] { 1 } else { 0 },
            "eqri" => self.rg[o] = if self.rg[ia] == ib { 1 } else { 0 },
            "eqrr" => self.rg[o] = if self.rg[ia] == self.rg[ib] { 1 } else { 0 },
            _ => panic!("bad op code!"),
        }
    }
}
