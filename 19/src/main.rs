use d1918::Watch;
use std::fs;

fn main() {
    let source = fs::read_to_string("input.txt").unwrap();
    let optimized_source = fs::read_to_string("optimized.txt").unwrap();

    let program = source.lines().collect::<Vec<_>>();
    let optimized = optimized_source.lines().collect::<Vec<_>>();

    let mut watch = Watch::new();
    watch.exec(&program);
    println!("Part 1: {}", watch.rg[0]);

    let mut watch = Watch::new();
    watch.rg = [1, 0, 0, 0, 0, 0];
    watch.exec(&optimized);
    println!("Part 2: {}", watch.rg[0]);
}
