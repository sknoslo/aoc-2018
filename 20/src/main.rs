use std::collections::{HashMap, HashSet};
use std::fs;

// const S1: &'static str = "^WNE$";
// const S2: &'static str = "^ENWWW(NEEE|SSE(EE|N))$";
// const S3: &'static str = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$";
// const S4: &'static str = "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$";
// const S5: &'static str = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$";

fn main() {
    // let sample = &S5.chars().collect::<Vec<_>>();
    let input = fs::read_to_string("input.txt")
        .unwrap()
        .chars()
        .collect::<Vec<_>>();

    let map = build_map(&input);
    let (p1, p2) = traverse(&map);
    println!("Part 1: {}", p1);
    println!("Part 2: {}", p2);
}

fn traverse(map: &RoomMap) -> (i32, i32) {
    let mut stack = Vec::new();
    let mut seen = HashSet::new();
    stack.push(((0, 0), 0));
    let mut max_len = 0;
    let mut long_paths = 0;

    while let Some((curr, len)) = stack.pop() {
        if seen.contains(&curr) {
            continue;
        }

        if len > max_len {
            max_len = len;
        }

        if len >= 1000 {
            long_paths += 1;
        }

        let room = &map[&curr];

        seen.insert(curr);

        if room.n == RoomSide::Door && !seen.contains(&(curr.0, curr.1 - 1)) {
            stack.push(((curr.0, curr.1 - 1), len + 1));
        }
        if room.w == RoomSide::Door && !seen.contains(&(curr.0 - 1, curr.1)) {
            stack.push(((curr.0 - 1, curr.1), len + 1));
        }
        if room.e == RoomSide::Door && !seen.contains(&(curr.0 + 1, curr.1)) {
            stack.push(((curr.0 + 1, curr.1), len + 1));
        }
        if room.s == RoomSide::Door && !seen.contains(&(curr.0, curr.1 + 1)) {
            stack.push(((curr.0, curr.1 + 1), len + 1));
        }
    }

    (max_len, long_paths)
}

fn build_map(directions: &[char]) -> RoomMap {
    let mut map = HashMap::new();
    let mut stack: Vec<(i32, i32)> = Vec::new();
    let mut cp = (0, 0);

    for d in directions.iter() {
        match d {
            '^' => {
                map.insert(cp, Room::new());
            }
            '$' => break,
            'N' => {
                let (x, y) = cp;
                let p = (x, y - 1);
                map.get_mut(&cp).unwrap().n = RoomSide::Door;

                if !map.contains_key(&p) {
                    map.insert(p, Room::new());
                }

                map.get_mut(&p).unwrap().s = RoomSide::Door;
                cp = p;
            }
            'W' => {
                let (x, y) = cp;
                let p = (x - 1, y);
                map.get_mut(&cp).unwrap().w = RoomSide::Door;

                if !map.contains_key(&p) {
                    map.insert(p, Room::new());
                }

                map.get_mut(&p).unwrap().e = RoomSide::Door;
                cp = p;
            }
            'E' => {
                let (x, y) = cp;
                let p = (x + 1, y);
                map.get_mut(&cp).unwrap().e = RoomSide::Door;

                if !map.contains_key(&p) {
                    map.insert(p, Room::new());
                }

                map.get_mut(&p).unwrap().w = RoomSide::Door;
                cp = p;
            }
            'S' => {
                let (x, y) = cp;
                let p = (x, y + 1);
                map.get_mut(&cp).unwrap().s = RoomSide::Door;

                if !map.contains_key(&p) {
                    map.insert(p, Room::new());
                }

                map.get_mut(&p).unwrap().n = RoomSide::Door;
                cp = p;
            }
            '(' => stack.push(cp),
            ')' => cp = stack.pop().unwrap(),
            '|' => cp = *stack.last().unwrap(),
            _ => panic!("unhandled direction"),
        }
    }

    map
}

type RoomMap = HashMap<(i32, i32), Room>;

#[derive(Debug)]
struct Room {
    n: RoomSide,
    w: RoomSide,
    e: RoomSide,
    s: RoomSide,
}

#[derive(Debug, Eq, PartialEq)]
enum RoomSide {
    Wall,
    Door,
}

impl Room {
    fn new() -> Room {
        Room {
            n: RoomSide::Wall,
            w: RoomSide::Wall,
            e: RoomSide::Wall,
            s: RoomSide::Wall,
        }
    }
}
