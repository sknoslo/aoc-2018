#[derive(Debug)]
pub struct Watch<'a> {
    pub ip: usize,
    pub ipr: usize,
    pub rg: [usize; 6],
    pub inst: Vec<Instruction<'a>>,
}

#[derive(Debug)]
pub struct Instruction<'a>(&'a str, usize, usize, usize);

impl<'a> Instruction<'a> {
    pub fn from_str(line: &str) -> Instruction {
        let parts = line.split_whitespace().collect::<Vec<&str>>();

        Instruction(
            parts[0],
            parts[1].parse().unwrap(),
            parts[2].parse().unwrap(),
            parts[3].parse().unwrap(),
        )
    }
}

pub enum Result {
    Halt,
    Cont,
}

impl<'a> Watch<'a> {
    pub fn new() -> Watch<'a> {
        Watch {
            ip: 0,
            ipr: 0,
            rg: [0; 6],
            inst: vec![]
        }
    }

    pub fn load(&mut self, program: &[&'a str]) {
        self.ipr = program[0].split_whitespace().collect::<Vec<&str>>()[1]
            .parse()
            .unwrap();

        for r in self.rg.iter_mut() {
            *r = 0;
        }

        self.inst = program[1..]
            .iter()
            .map(|line| Instruction::from_str(line))
            .collect();
    }

    pub fn proc(&mut self) -> Result {

        self.rg[self.ipr] = self.ip;

        let &Instruction(op, ia, ib, o) = &self.inst[self.ip];

        match op {
            "addr" => self.rg[o] = self.rg[ia] + self.rg[ib],
            "addi" => self.rg[o] = self.rg[ia] + ib,
            "mulr" => self.rg[o] = self.rg[ia] * self.rg[ib],
            "muli" => self.rg[o] = self.rg[ia] * ib,
            "banr" => self.rg[o] = self.rg[ia] & self.rg[ib],
            "bani" => self.rg[o] = self.rg[ia] & ib,
            "borr" => self.rg[o] = self.rg[ia] | self.rg[ib],
            "bori" => self.rg[o] = self.rg[ia] | ib,
            "setr" => self.rg[o] = self.rg[ia],
            "seti" => self.rg[o] = ia,
            "gtir" => self.rg[o] = if ia > self.rg[ib] { 1 } else { 0 },
            "gtri" => self.rg[o] = if self.rg[ia] > ib { 1 } else { 0 },
            "gtrr" => self.rg[o] = if self.rg[ia] > self.rg[ib] { 1 } else { 0 },
            "eqir" => self.rg[o] = if ia == self.rg[ib] { 1 } else { 0 },
            "eqri" => self.rg[o] = if self.rg[ia] == ib { 1 } else { 0 },
            "eqrr" => self.rg[o] = if self.rg[ia] == self.rg[ib] { 1 } else { 0 },
            _ => panic!("bad op code!"),
        }

        self.ip = self.rg[self.ipr] + 1;

        if self.inst.get(self.ip).is_none() {
            Result::Halt
        } else {
            Result::Cont
        }
    }
}
