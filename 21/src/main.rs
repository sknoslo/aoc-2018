use d2118::Watch;
use d2118::Result::{Cont};
use std::collections::HashSet;
use std::fs;

fn main() {
    let source = fs::read_to_string("input.txt").unwrap();

    let program = source.lines().collect::<Vec<_>>();

    let mut watch = Watch::new();
    println!("Part 1: {}", do_part_one(&mut watch, &program));
    let mut watch = Watch::new();
    println!("Part 2: {}", do_part_two(&mut watch, &program));
}

fn do_part_one<'a>(watch: &'a mut Watch<'a>, program: &[&'a str]) -> usize {
    watch.load(&program);

    // line 28 looks like an exit condition, when rg[0] == rg[3]
    while watch.ip != 28 {
        watch.proc();
    }

    let first_exit_cond = watch.rg[3];
    watch.rg[0] = first_exit_cond;

    // continue and see if it exits
    while let Cont = watch.proc() {}

    first_exit_cond
}

fn do_part_two<'a>(watch: &'a mut Watch<'a>, program: &[&'a str]) -> usize {
    // seems like a psuedo-random number generator, or something.
    // which means it should cycle at some point.
    // the answer to part 2 should be the last value of rg3
    // that hadn't been seen before.
    let mut seen = HashSet::new();
    watch.load(&program);

    let mut last_exit_cond = 0;

    while let Cont = watch.proc() {
        if watch.ip == 28 {
            if !seen.insert(watch.rg[3]) {
                break;
            }
            last_exit_cond = watch.rg[3];
        }
    }

    last_exit_cond
}
