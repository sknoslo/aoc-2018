use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Debug)]
pub enum RegionType {
    Rocky,
    Wet,
    Narrow,
}

pub struct Scan {
    pub depth: usize,
    pub target: (usize, usize),
    cache: HashMap<(usize, usize), usize>,
}

impl Scan {
    pub fn new(depth: usize, target_x: usize, target_y: usize) -> Scan {
        Scan {
            depth,
            target: (target_x, target_y),
            cache: HashMap::new(),
        }
    }

    pub fn get_erosion_level(&mut self, x: usize, y: usize) -> usize {
        let coords = (x, y);

        if self.cache.contains_key(&coords) {
            self.cache[&coords]
        } else {
            let geo_index;
            if (x == 0 && y == 0) || (x == self.target.0 && y == self.target.1) {
                geo_index = 0;
            } else if y == 0 {
                geo_index = x * 16807;
            } else if x == 0 {
                geo_index = y * 48271;
            } else {
                geo_index = self.get_erosion_level(x - 1, y) * self.get_erosion_level(x, y - 1);
            }

            let erosion_level = (geo_index + self.depth) % 20183;
            self.cache.insert(coords, erosion_level);
            erosion_level
        }
    }

    pub fn get_region_type(&mut self, x: usize, y: usize) -> RegionType {
        match self.get_erosion_level(x, y) % 3 {
            0 => RegionType::Rocky,
            1 => RegionType::Wet,
            _ => RegionType::Narrow,
        }
    }

    pub fn to_string(&mut self, x: usize, y: usize) -> String {
        let mut printed = String::new();
        for y in 0..y + 1 {
            for x in 0..x + 1 {
                if (x, y) == (0, 0) {
                    printed.push('M');
                } else if (x, y) == (self.target.0, self.target.1) {
                    printed.push('T');
                } else {
                    printed.push(match self.get_region_type(x, y) {
                        RegionType::Rocky => '.',
                        RegionType::Wet => '=',
                        RegionType::Narrow => '|',
                    });
                }
            }
            printed.push_str("\n");
        }

        printed
    }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, PartialEq, Eq, Hash)]
pub enum Tool {
    Nothing,
    Torch,
    ClimbingGear,
}

#[derive(Debug, Ord)]
pub struct OpenRegion {
    pub x: usize,
    pub y: usize,
    pub cost: usize,
    pub estimated: usize,
    pub tool: Tool,
    pub from: Option<(usize, usize, usize, Tool)>,
}

impl OpenRegion {
    pub fn new(
        x: usize,
        y: usize,
        cost: usize,
        tool: Tool,
        tx: usize,
        ty: usize,
        from: Option<(usize, usize, usize, Tool)>,
    ) -> OpenRegion {
        let estimated = (tx as isize - x as isize).abs() as usize
            + (ty as isize - y as isize).abs() as usize
            + cost;

        OpenRegion {
            x,
            y,
            cost,
            estimated,
            tool,
            from,
        }
    }
}

impl PartialOrd for OpenRegion {
    fn partial_cmp(&self, other: &OpenRegion) -> Option<Ordering> {
        if self.estimated < other.estimated {
            Some(Ordering::Greater)
        } else if self.estimated == other.estimated {
            Some(Ordering::Equal)
        } else {
            Some(Ordering::Less)
        }
    }
}

impl PartialEq for OpenRegion {
    fn eq(&self, other: &OpenRegion) -> bool {
        self.estimated == other.estimated
    }
}

impl Eq for OpenRegion {}
