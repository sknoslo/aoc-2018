use d2218::OpenRegion;
use d2218::RegionType::{Narrow, Rocky, Wet};
use d2218::Scan;
use d2218::Tool;
use d2218::Tool::{ClimbingGear, Nothing, Torch};
use std::collections::{BinaryHeap, HashMap};

// Sample
// const DEPTH: usize = 510;
// const TARGET_X: usize = 10;
// const TARGET_Y: usize = 10;

// Input
const DEPTH: usize = 3879;
const TARGET_X: usize = 8;
const TARGET_Y: usize = 713;

fn main() {
    let mut map = Scan::new(DEPTH, TARGET_X, TARGET_Y);

    println!("Part 1: {}", do_part_one(&mut map));
    println!("Part 2: {}", do_part_two(&mut map));
}

fn do_part_one(map: &mut Scan) -> usize {
    let mut danger = 0;

    for y in 0..TARGET_Y + 1 {
        for x in 0..TARGET_X + 1 {
            danger += map.get_region_type(x, y) as usize;
        }
    }

    danger
}

const DIRS: [(isize, isize); 4] = [(0, 1), (1, 0), (0, -1), (-1, 0)];

fn do_part_two(map: &mut Scan) -> usize {
    // use binary heap as a priority queue
    let mut open_regions = BinaryHeap::new();
    // to keep track of the x,y coords visted and with which tool
    let mut visited = HashMap::new();

    open_regions.push(OpenRegion::new(0, 0, 0, Torch, TARGET_X, TARGET_Y, None));

    let mut lowest_cost = 0;
    while let Some(region) = open_regions.pop() {
        let OpenRegion {
            x,
            y,
            cost,
            tool,
            from,
            ..
        } = region;

        if visited.contains_key(&(x, y, cost, tool)) {
            continue;
        }
        visited.insert((x, y, cost, tool), from);

        if x == TARGET_X && y == TARGET_Y {
            print_path(&visited, (x, y, cost, tool));
            if tool != Torch {
                open_regions.push(OpenRegion::new(
                    x,
                    y,
                    cost + 7,
                    Torch,
                    TARGET_X,
                    TARGET_Y,
                    Some((x, y, cost, tool)),
                ));
                continue;
            } else {
                lowest_cost = cost;
                break;
            }
        }

        for (dx, dy) in DIRS.iter() {
            let nx = x as isize + dx;
            let ny = y as isize + dy;
            if nx < 0 || ny < 0 {
                continue;
            }
            let nx = nx as usize;
            let ny = ny as usize;

            let curr_region = map.get_region_type(x, y);
            let next_region = map.get_region_type(nx, ny);

            match next_region {
                Rocky => {
                    if tool == Torch || tool == ClimbingGear {
                        open_regions.push(OpenRegion::new(
                            nx,
                            ny,
                            cost + 1,
                            tool,
                            TARGET_X,
                            TARGET_Y,
                            Some((x, y, cost, tool)),
                        ));
                    } else {
                        match curr_region {
                            Wet => {
                                open_regions.push(OpenRegion::new(
                                    x,
                                    y,
                                    cost + 7,
                                    ClimbingGear,
                                    TARGET_X,
                                    TARGET_Y,
                                    Some((x, y, cost, tool)),
                                ));
                            }
                            Narrow => {
                                open_regions.push(OpenRegion::new(
                                    x,
                                    y,
                                    cost + 7,
                                    Torch,
                                    TARGET_X,
                                    TARGET_Y,
                                    Some((x, y, cost, tool)),
                                ));
                            }
                            _ => {} // lies
                        }
                    }
                }
                Wet => {
                    if tool == Nothing || tool == ClimbingGear {
                        open_regions.push(OpenRegion::new(
                            nx,
                            ny,
                            cost + 1,
                            tool,
                            TARGET_X,
                            TARGET_Y,
                            Some((x, y, cost, tool)),
                        ));
                    } else {
                        match curr_region {
                            Rocky => {
                                open_regions.push(OpenRegion::new(
                                    x,
                                    y,
                                    cost + 7,
                                    ClimbingGear,
                                    TARGET_X,
                                    TARGET_Y,
                                    Some((x, y, cost, tool)),
                                ));
                            }
                            Narrow => {
                                open_regions.push(OpenRegion::new(
                                    x,
                                    y,
                                    cost + 7,
                                    Nothing,
                                    TARGET_X,
                                    TARGET_Y,
                                    Some((x, y, cost, tool)),
                                ));
                            }
                            _ => {} // lies
                        }
                    }
                }
                Narrow => {
                    if tool == Nothing || tool == Torch {
                        open_regions.push(OpenRegion::new(
                            nx,
                            ny,
                            cost + 1,
                            tool,
                            TARGET_X,
                            TARGET_Y,
                            Some((x, y, cost, tool)),
                        ));
                    } else {
                        match curr_region {
                            Rocky => {
                                open_regions.push(OpenRegion::new(
                                    x,
                                    y,
                                    cost + 7,
                                    Torch,
                                    TARGET_X,
                                    TARGET_Y,
                                    Some((x, y, cost, tool)),
                                ));
                            }
                            Wet => {
                                open_regions.push(OpenRegion::new(
                                    x,
                                    y,
                                    cost + 7,
                                    Nothing,
                                    TARGET_X,
                                    TARGET_Y,
                                    Some((x, y, cost, tool)),
                                ));
                            }
                            _ => {} // lies
                        }
                    }
                }
            }
        }
    }

    lowest_cost
}

fn print_path(
    visited: &HashMap<(usize, usize, usize, Tool), Option<(usize, usize, usize, Tool)>>,
    mut end: (usize, usize, usize, Tool),
) {
    println!("{}, {} -> {:?}", end.0, end.1, end.2);
    while let Some(Some((x, y, cost, tool))) = visited.get(&end) {
        println!("{}, {} -> {:?}", x, y, tool);
        end = (*x, *y, *cost, *tool);
    }
}
