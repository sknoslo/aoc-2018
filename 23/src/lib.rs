use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug)]
pub struct NanoBot {
    pub pos: Point,
    pub radius: isize,
}

#[derive(Debug)]
pub struct Region {
    pub center: Point,
    pub radius: isize,
}

#[derive(Debug, Copy, Clone)]
pub struct Point {
    pub x: isize,
    pub y: isize,
    pub z: isize,
}

impl NanoBot {
    pub fn new(x: isize, y: isize, z: isize, radius: isize) -> NanoBot {
        NanoBot {
            pos: Point::new(x, y, z),
            radius,
        }
    }

    pub fn from_str(input: &str) -> NanoBot {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)").unwrap();
        }

        let captures = RE.captures(input).unwrap();

        NanoBot::new(
            captures[1].parse().unwrap(),
            captures[2].parse().unwrap(),
            captures[3].parse().unwrap(),
            captures[4].parse().unwrap(),
        )
    }

    pub fn in_range_of(&self, other: &NanoBot) -> bool {
        self.pos.dist_to(&other.pos) <= self.radius
    }
}

impl Point {
    pub fn new(x: isize, y: isize, z: isize) -> Point {
        Point { x, y, z }
    }

    pub fn dist_to(&self, other: &Point) -> isize {
        (other.x - self.x).abs() + (other.y - self.y).abs() + (other.z - self.z).abs()
    }
}

impl Region {
    pub fn new(x: isize, y: isize, z: isize, radius: isize) -> Region {
        Region {
            center: Point::new(x, y, z),
            radius,
        }
    }

    pub fn in_range_of(&self, bot: &NanoBot) -> bool {
        self.center.dist_to(&bot.pos) <= bot.radius + self.radius
    }
}
