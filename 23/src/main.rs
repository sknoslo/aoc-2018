use d2318::{NanoBot, Point, Region};
use std::fs;

fn main() {
    // let input = fs::read_to_string("sample.txt").unwrap();
    let input = fs::read_to_string("input.txt").unwrap();

    let bots = input.lines().map(NanoBot::from_str).collect::<Vec<_>>();

    println!("Part 1: {}", do_part_one(&bots));
    println!("Part 2: {}", do_part_two(&bots));
}

fn do_part_one(bots: &[NanoBot]) -> isize {
    let mut largest = &bots[0];

    for bot in bots.iter() {
        if bot.radius > largest.radius {
            largest = bot;
        }
    }

    let mut in_range = 0;

    for bot in bots.iter() {
        if largest.in_range_of(bot) {
            in_range += 1;
        }
    }

    in_range
}

fn do_part_two(bots: &[NanoBot]) -> isize {
    let my_pos = Point::new(0, 0, 0);

    let mut best_in_range = 0;
    let mut best_dist_to_me = isize::max_value();

    // These numbers came from manually shrinking the best region(s) from a radius
    // of 10_000_000 all the way down to 0. Surely, this could have been automated,
    // but it was faster for me to just do it manually.
    for x in 59_110_513..59_110_516 {
        for y in 46_561_284..46_561_287 {
            for z in 36_801_701..36_801_704 {
                let region = Region::new(x, y, z, 0);
                let dist_to_me = region.center.dist_to(&my_pos);
                let mut in_range = 0;
                for bot in bots {
                    if region.in_range_of(&bot) {
                        in_range += 1;
                    }
                }

                if in_range > best_in_range {
                    best_in_range = in_range;
                    best_dist_to_me = dist_to_me;
                } else if in_range == best_in_range && dist_to_me < best_dist_to_me {
                    best_dist_to_me = dist_to_me;
                }
            }
        }
    }

    best_dist_to_me
}
