use lazy_static::lazy_static;
use regex::Regex;
use std::fmt;

#[derive(Debug, Eq, PartialEq)]
pub enum GroupType {
    ImmuneSystem,
    Infection,
}

pub struct Group {
    pub t: GroupType,
    pub units: i32,
    pub hp: i32,
    pub ad: i32,
    pub weaknesses: Vec<String>,
    pub immunities: Vec<String>,
    pub initiative: i32,
    pub attack_type: String,
}

impl fmt::Debug for Group {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "[{:?}] {{ u: {}, hp: {}, ad: {}, i: {}, at: {}, weak: {:?}, immun: {:?} }}",
            self.t,
            self.units,
            self.hp,
            self.ad,
            self.initiative,
            self.attack_type,
            self.weaknesses,
            self.immunities
        )
    }
}

impl Group {
    pub fn from_str(input: &str, t: GroupType) -> Group {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"^(\d+)[a-z ]+(\d+)[a-z ]+(\((.*)\))?[a-z ]+(\d+) ([a-z]+) [a-z ]+(\d+)$"
            )
            .unwrap();
        }

        let cap = RE.captures(&input).expect(input);

        let mut weaknesses = Vec::new();
        let mut immunities = Vec::new();

        match cap.get(4) {
            Some(m) => {
                for s in m.as_str().split("; ") {
                    if s.starts_with("immune to") {
                        for i in s.replace("immune to ", "").split(", ") {
                            immunities.push(i.to_string());
                        }
                    } else {
                        for i in s.replace("weak to ", "").split(", ") {
                            weaknesses.push(i.to_string());
                        }
                    }
                }
            }
            None => {}
        }

        Group {
            t,
            units: cap[1].parse().unwrap(),
            hp: cap[2].parse().unwrap(),
            ad: cap[5].parse().unwrap(),
            weaknesses,
            immunities,
            initiative: cap[7].parse().unwrap(),
            attack_type: cap[6].to_string(),
        }
    }

    pub fn effective_power(&self) -> i32 {
        self.ad * self.units
    }

    pub fn enemy_attack_damage(&self, effective_power: i32, attack_type: &String) -> i32 {
        if self.immunities.contains(&attack_type) {
            0
        } else if self.weaknesses.contains(&attack_type) {
            2 * effective_power
        } else {
            effective_power
        }
    }
}

#[derive(Debug)]
pub struct AttackPlan {
    pub initiative: i32,
    pub target: i32,
}
