use d2418::AttackPlan;
use d2418::Group;
use d2418::GroupType;
use d2418::GroupType::{ImmuneSystem, Infection};
use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::fs;

type GroupList = HashMap<i32, Group>;

fn main() {
    let input = fs::read_to_string("input.txt").unwrap();

    let mut groups = build_group_list(&input, 0);

    println!("Part 1: {}", do_part_one(&mut groups));
    println!("Part 2: {}", do_part_two(&input));
}

fn build_group_list(input: &str, immune_boost: i32) -> GroupList {
    let teams = input.split("\n\n").collect::<Vec<_>>();

    let mut groups = HashMap::new();

    for line in teams[0].lines().skip(1) {
        let mut g = Group::from_str(line, ImmuneSystem);

        g.ad += immune_boost;
        groups.insert(g.initiative, g);
    }

    for line in teams[1].lines().skip(1) {
        let g = Group::from_str(line, Infection);

        groups.insert(g.initiative, g);
    }

    groups
}

fn do_part_one(mut groups: &mut GroupList) -> i32 {
    let (_, result) = battle(&mut groups).unwrap();

    result
}

fn do_part_two(input: &str) -> i32 {
    let mut immune_boost = 1;
    loop {
        let mut groups = build_group_list(&input, immune_boost);
        match battle(&mut groups) {
            Some((ImmuneSystem, result)) => return result,
            Some((Infection, _)) | None => {}
        }
        immune_boost += 1;
    }
}

fn battle(groups: &mut GroupList) -> Option<(GroupType, i32)> {
    let mut chosen = HashSet::with_capacity(groups.len());
    let mut attack_plans = Vec::with_capacity(groups.len());

    loop {
        chosen.clear();
        attack_plans.clear();

        let mut values = groups.values().collect::<Vec<_>>();
        values.sort_by(|a, b| {
            let aep = a.effective_power();
            let bep = b.effective_power();
            if bep > aep {
                Ordering::Greater
            } else if bep == aep && b.initiative > a.initiative {
                Ordering::Greater
            } else {
                Ordering::Less
            }
        });

        for i in 0..values.len() {
            let mut to_attack = None;
            for j in 0..values.len() {
                let ji = values[j].initiative;

                if values[i].t == values[j].t || chosen.contains(&ji) {
                    continue;
                }

                let damage = values[j]
                    .enemy_attack_damage(values[i].effective_power(), &values[i].attack_type);

                let jep = values[j].effective_power();

                if damage > 0 {
                    match to_attack {
                        None => to_attack = Some((ji, damage, jep)),
                        Some((i, d, ep)) => {
                            if damage > d
                                || (damage == d && jep > ep)
                                || (damage == d && jep == ep && ji > i)
                            {
                                to_attack = Some((ji, damage, jep));
                            }
                        }
                    }
                }
            }

            match to_attack {
                None => {} // nothing to attack womp, womp.
                Some((ti, _, _)) => {
                    chosen.insert(ti);
                    attack_plans.push(AttackPlan {
                        initiative: values[i].initiative,
                        target: ti,
                    });
                }
            }
        }

        attack_plans.sort_by(|a, b| b.initiative.cmp(&a.initiative));

        let mut stalemate = true;
        for ap in attack_plans.iter() {
            if !groups.contains_key(&ap.initiative) {
                continue;
            }

            let attacker = &groups[&ap.initiative];
            let target = &groups[&ap.target];
            let damage =
                target.enemy_attack_damage(attacker.effective_power(), &attacker.attack_type);

            groups.entry(ap.target).and_modify(|g| {
                let units_to_remove = damage / g.hp;

                if units_to_remove > 0 {
                    stalemate = false;
                }

                g.units -= units_to_remove;
            });

            if groups.get(&ap.target).unwrap().units <= 0 {
                groups.remove(&ap.target);
            }
        }

        if stalemate {
            return None; // no winners in a stalemate?
        }

        if groups.iter().filter(|(_k, v)| v.t == ImmuneSystem).count() < 1 {
            return Some((Infection, groups.iter().map(|(_, v)| v.units).sum()));
        }

        if groups.iter().filter(|(_k, v)| v.t == Infection).count() < 1 {
            return Some((ImmuneSystem, groups.iter().map(|(_, v)| v.units).sum()));
        }
    }
}
