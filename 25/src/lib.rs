#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq)]
pub struct Point {
    x: i32,
    y: i32,
    z: i32,
    t: i32,
}

impl Point {
    pub fn from_str(input: &str) -> Point {
        let vals = input.split(',').collect::<Vec<_>>();

        Point {
            x: vals[0].parse().unwrap(),
            y: vals[1].parse().unwrap(),
            z: vals[2].parse().unwrap(),
            t: vals[3].parse().unwrap(),
        }
    }

    pub fn close_to(&self, other: &Point) -> bool {
        (self.x - other.x).abs()
            + (self.y - other.y).abs()
            + (self.z - other.z).abs()
            + (self.t - other.t).abs()
            <= 3
    }
}
