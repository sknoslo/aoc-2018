use d2518::Point;
use std::collections::{HashMap, HashSet};
use std::fs;

fn main() {
    let input = fs::read_to_string("input.txt").unwrap();

    let points = input.lines().map(Point::from_str).collect::<Vec<_>>();

    println!("Part 1: {}", do_part_one(&points));
}

fn do_part_one(points: &[Point]) -> u32 {
    let mut constellations = 0;

    let mut visited = HashSet::new();
    let mut adjacent: HashMap<Point, Vec<Point>> = HashMap::new();

    for a in points.iter() {
        for b in points.iter() {
            if a.close_to(b) {
                adjacent
                    .entry(*a)
                    .and_modify(|c| c.push(*b))
                    .or_insert(vec![*b]);
            }
        }
    }

    let mut stack = Vec::new();
    for point in points.iter() {
        if !visited.contains(point) {
            constellations += 1;
        } else {
            continue;
        }

        stack.push(point);

        while let Some(point) = stack.pop() {
            if !visited.insert(point) {
                continue;
            }

            for close_point in adjacent[&point].iter() {
                stack.push(close_point);
            }
        }
    }

    constellations
}
